import click
from subprocess import check_output


@click.command()
def cli():
    command = "git rev-parse --short HEAD".split(" ")
    out = check_output(command)

    with open(".env", "w") as envfile:
        envfile.write("GIT_VERSION='{}'".format(out.strip().decode("utf-8")))
